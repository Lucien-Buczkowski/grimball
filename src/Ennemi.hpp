#pragma once
#include "Personnage.hpp"
#include "Position.hpp"
#include <iostream>
#include <SFML/Graphics.hpp>

class Ennemi : public Personnage {
	private :
        sf::Texture texture_O_;
        sf::Texture texture_E_;

	public :
		// Constructeur
		Ennemi(sf::RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E);

		// Destructeur virtuel car la aussi il y a des sous classe à moins que cela ne soit pas nécessaire.
		virtual ~ Ennemi() {}

		// Fonctions de déplacement en direction des 4 points cardinaux
		void deplacement_N(); // Nord
		void deplacement_S(); // Sud
		void deplacement_E(); // Est
		void deplacement_O(); // Ouest
		
		// Fonction de déplacement (IA des mobs)
		
		void evolue(int x_cible, int y_cible);

		// Méthode de dessin virtuelle pure(on ne sais pas dessiner un Ennemi)
		// Avec cette fonction la classe Ennemi est abstraite et on ne peut donc pas créer d'Ennemi
		virtual void dessine() = 0;
		void recommence();
};
