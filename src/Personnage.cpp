#include "Personnage.hpp"
#include <SFML/Graphics.hpp>
#include "Position.hpp"
#include <string>
#include <iostream>

using namespace sf;

Personnage::Personnage(RenderWindow* fenetre, Position pos)
	:fenetre_(fenetre)
	{
		pos_ = pos;
        pos_initiale_ = pos;
		portee_ = 0;
		point_attaque_ = 0;
		point_de_vie_ = 0;
		nom_ = "Personnage";
		est_en_vie_ = true;
			}

Position Personnage::get_pos() {
	return pos_;
}

int Personnage::get_pos_x(){
	return pos_.valeur_x();
}

int Personnage::get_pos_y() {
	return pos_.valeur_y();
}

std::string Personnage::get_nom() {
	return nom_;
}

int Personnage::get_PDV() {
	return point_de_vie_;
}

int Personnage::get_PA(){
	return point_attaque_;
}

int Personnage::get_taille_x(){
	return taille_x_;
}

int Personnage::get_taille_y() {
	return taille_y_;
}

int Personnage::get_portee() {
	return portee_;
}

int Personnage::get_vitesse() {
	return vitesse_;
}

bool Personnage::est_vivant() {
	return est_en_vie_;
}

void Personnage::set_fenetre(RenderWindow* fenetre){
	if(fenetre != nullptr){
		fenetre_ = fenetre;
	}
}

RenderWindow* Personnage::get_fenetre(){
	return fenetre_;
}

Position Personnage::get_position_initiale(){
    return pos_initiale_;
}

void Personnage::set_vivant(bool vie){
	est_en_vie_ = vie;
}

void Personnage::enlever_PV(int degat){
	point_de_vie_-= degat;
	if(point_de_vie_ <= 0){
		est_en_vie_ = false;
	}
}

sf::Sprite Personnage::get_sprite(){
    return sprite_du_personnage_;
}
