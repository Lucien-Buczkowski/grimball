#include "Position.hpp"
#include <SFML/Graphics.hpp>
#include "Balle.hpp"
#include "constantes.hpp"
#include "Ennemi.hpp"
#include <stdio.h>

using namespace sf;

Balle::Balle(RenderWindow* fenetre, Position pos)
	:fenetre_(fenetre)
	{
		pos_ = pos;
        vitesse_x_ = 0;
        vitesse_y_ = 0;
		sprite_balle_.setOrigin(LARGEUR_BALLE/2, LARGEUR_BALLE/2);
		sprite_balle_.scale(1,1);
	}

int Balle::get_vitesse_x(){
	return vitesse_x_;
}

int Balle::get_vitesse_y(){
	return vitesse_y_;
}

void Balle::set_vitesse_x(int vit_x){
	vitesse_x_ = vit_x;
}

void Balle::set_vitesse_y(int vit_y){
	vitesse_y_ = vit_y;
}

Position Balle::get_position(){
    return pos_;
}

void Balle::evolue(){
    int x = pos_.valeur_x() + vitesse_x_;
    int y = pos_.valeur_y() + vitesse_y_;
    pos_.set_x(x);
    pos_.set_y(y);
    if(x > LARGEUR_FENETRE - LARGEUR_BALLE - LARGEUR_MUR || x < LARGEUR_MUR){
        //Touche � droite ou � gauche
        vitesse_x_ = -vitesse_x_;
    }

    if(y < LARGEUR_BALLE/2 || y > HAUTEUR_FENETRE - LARGEUR_BALLE - LARGEUR_MUR*2){
        //Touche en haut ou en bas
        vitesse_y_ = -vitesse_y_;
    }
sprite_balle_.setPosition(pos_.valeur_x(), pos_.valeur_y());
}

void Balle::set_Position(int x, int y){
	pos_.set_x(x);
	pos_.set_y(y);
}

void Balle::dessine(){
    Texture textureBalle;
    textureBalle.loadFromFile(BALLE);
    sprite_balle_.setTexture(textureBalle);
	sprite_balle_.setPosition(pos_.valeur_x(), pos_.valeur_y());
	fenetre_->draw(sprite_balle_);

}

sf::Sprite Balle::get_sprite(){
    return sprite_balle_;
}

int Balle::get_x(){
	return pos_.valeur_x();
}

int Balle::get_y(){
	return pos_.valeur_y();
}
