#include "Ennemi.hpp"
#include "Personnage.hpp"
#include "Position.hpp"
#include "constantes.hpp"
#include <iostream>
#include <SFML/Graphics.hpp>


Ennemi::Ennemi(sf::RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E)
    :Personnage(fenetre, pos)
{
    point_attaque_ = 0;
    point_de_vie_ = 0;
    portee_ = 0;
    nom_ ="Ennemi";
    est_en_vie_ =true;
    sf::Texture texture_O;
    texture_O.loadFromFile(texture_chemin_O);
    texture_O_ = texture_O;
    sf::Texture texture_E;
    texture_E.loadFromFile(texture_chemin_E);
    texture_E_ = texture_E;
    sprite_du_personnage_.setTexture(texture_E_);
}

void Ennemi::recommence(){
    est_en_vie_ = true;
    pos_ = pos_initiale_;
}

void Ennemi::deplacement_E()
{
    pos_.set_x(pos_.valeur_x() + vitesse_);
    sprite_du_personnage_.setTexture(texture_E_);
}

void Ennemi::deplacement_O()
{
    pos_.set_x(pos_.valeur_x() - vitesse_);
    sprite_du_personnage_.setTexture(texture_O_);
}

void Ennemi::deplacement_N()
{
    pos_.set_y(pos_.valeur_y() - vitesse_);
}

void Ennemi::deplacement_S()
{
    pos_.set_y(pos_.valeur_y() + vitesse_);
}

void Ennemi::evolue(int x_cible, int y_cible)
{
    if(est_en_vie_)
    {


    int hauteur = y_cible - pos_.valeur_y();
    int longueur = x_cible - pos_.valeur_x();

    if(hauteur > 0 )
    {
        this->deplacement_S();
    }

    if(hauteur < 0 )
    {
        this->deplacement_N();
    }

    if(longueur < 0)
    {
        this->deplacement_O();
    }

    if(longueur > 0)
    {
        this->deplacement_E();
    }
    
	}
}
