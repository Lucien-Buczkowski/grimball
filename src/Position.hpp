#pragma once

// Classe représentant la position en pixel (int) sur R²
class Position {
	private :
		int x_;
		int y_;

	public :
		//Constructeur
		Position(int x = 0, int y = 0);

		// getter pour la valeur x
		int valeur_x();
		// getter pour la valeur y
		int valeur_y();

		// setters
		void set_x(int x);
		void set_y(int y);
		
		// méthode pour ajouter un entier a une coordonnée
		void ajoute_x(int x);
		void ajoute_y(int y);
		
		// méthode pour retourner une approximation de la norme euclidienne
		int norme();

};

// définition de l'opérateur + pour pouvoir additionner deux positions
Position operator +(Position p1, Position p2);

// définition de l'opérateur - pour pouvoir soustraire deux positions
Position operator -(Position p1, Position p2);
