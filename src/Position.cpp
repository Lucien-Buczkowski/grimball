#include "Position.hpp"
#include <math.h>

Position::Position(int x, int y)
	:x_(x), y_(y)
	{}
int Position::valeur_x(){
	return x_;
}

int Position::valeur_y(){
	return y_;
}

void Position::set_x(int x){
	x_ = x;
}

void Position::set_y(int y){
	y_ = y;
}

void Position::ajoute_x(int x){
	x_ += x;
}

void Position::ajoute_y(int y){
	y_ += y;
}

int Position::norme(){
	double n = sqrt((x_*x_)+(y_*y_));
	n = n + 0.5;
	int resultat(n);
	return resultat;
}


Position operator +(Position p1, Position p2){
	Position p(p1.valeur_x() + p2.valeur_x(), p1.valeur_y() + p2.valeur_y());
	return p;
}

Position operator -(Position p1, Position p2){
	Position p(p1.valeur_x() - p2.valeur_x(), p1.valeur_y() - p2.valeur_y());
	return p;
}
