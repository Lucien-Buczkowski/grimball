#include <SFML/Graphics.hpp>
#ifndef MENUS_HPP_INCLUDED
#define MENUS_HPP_INCLUDED
#include "Position.hpp"
#include "constantes.hpp"

int menuPrincipal( int taille_boutons, sf::RenderWindow* fenetre);

typedef struct{

Position pos;
int taille;
int action=0;

} Bouton_Menu;
#endif // MENUS_HPP_INCLUDED
