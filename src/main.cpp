#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include "constantes.hpp"
#include "Position.hpp"
#include "Balle.hpp"
#include "Ennemi.hpp"
#include "Ennemi_Degats.hpp"
#include "Ennemi_Normal.hpp"
#include "Ennemi_Tank.hpp"
#include "Personnage.hpp"
#include "Position.hpp"
#include "Vague.hpp"
#include "menus.hpp"
#include <vector>
#include <SFML/Audio.hpp>
using namespace sf;
using namespace std;

// La variable etat permet au jeu de tourner tant que etat != 1
// et passe � 1 pour fermer le jeu quand le bouton "quitter" est press�
int etat = ETAT_MENU;


int main()
{
    bool a_gagne = false, a_perdu=false;
    bool est_initialise = true;
    int numeroVague = 0;
    Texture textureCarte;
    textureCarte.loadFromFile(TEXTURE_CARTE);
    Sprite carte(textureCarte);
    carte.setPosition(0,0);

    Music theme_pcpal;
    theme_pcpal.openFromFile(THEME_PCPAL);

    Font font;
    if(!font.loadFromFile(POLICE)){
        printf("Error");
    }


    RenderWindow* fenetre = new RenderWindow (VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Grimball");
    fenetre->setFramerateLimit(60);
    Position pos(150, 200);
    Position posBalle(LARGEUR_FENETRE/2,HAUTEUR_FENETRE/2);

    Balle balle_1(fenetre, posBalle);

    Joueur joueur_1(fenetre,pos, new Balle (balle_1));
    Position posDroite(LARGEUR_FENETRE, HAUTEUR_FENETRE/2);
    Position posHaut(LARGEUR_FENETRE/2, 0);
    Position posGauche(0, HAUTEUR_FENETRE/2);
    Position posBas(LARGEUR_FENETRE/2, HAUTEUR_FENETRE);


    Vague vague(fenetre, new Joueur (joueur_1), 1);

    Ennemi_Normal ennemi_11(fenetre, posHaut, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);
    Ennemi_Normal ennemi_12(fenetre, posGauche, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);
    Ennemi_Tank ennemi_13(fenetre, posDroite, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);

    vague.add_Ennemi(new Ennemi_Normal(ennemi_11));
    vague.add_Ennemi(new Ennemi_Normal(ennemi_12));
    vague.add_Ennemi(new Ennemi_Tank(ennemi_13));

    Ennemi_Normal ennemi_21(fenetre, posHaut, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);
    Ennemi_Normal ennemi_22(fenetre, posBas, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);
    Ennemi_Tank ennemi_23(fenetre, posDroite, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);
    Ennemi_Normal ennemi_24(fenetre, posGauche, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);


    Ennemi_Degats ennemi_31(fenetre, posHaut, ENNEMI_DEGATS_GAUCHE, ENNEMI_DEGATS_DROITE);
    Ennemi_Tank ennemi_32(fenetre, posDroite, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);
    Ennemi_Normal ennemi_33(fenetre, posGauche, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);


    Ennemi_Degats ennemi_41(fenetre, posHaut, ENNEMI_DEGATS_GAUCHE, ENNEMI_DEGATS_DROITE);
    Ennemi_Tank ennemi_42(fenetre, posDroite, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);
    Ennemi_Normal ennemi_43(fenetre, posGauche, ENNEMI_NORMAL_GAUCHE, ENNEMI_NORMAL_DROITE);
    Ennemi_Tank ennemi_44(fenetre, posBas, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);


    Ennemi_Degats ennemi_51(fenetre, posHaut, ENNEMI_DEGATS_GAUCHE, ENNEMI_DEGATS_DROITE);
    Ennemi_Tank ennemi_52(fenetre, posDroite, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);
    Ennemi_Tank ennemi_53(fenetre, posGauche, ENNEMI_TANK_GAUCHE, ENNEMI_TANK_DROITE);
    Ennemi_Degats ennemi_54(fenetre, posBas, ENNEMI_DEGATS_GAUCHE, ENNEMI_DEGATS_DROITE);

    theme_pcpal.play();
    theme_pcpal.setVolume(5.f);
    theme_pcpal.setLoop(true);
    do
    {
        fenetre->clear();
        Event event;
        if(etat == ETAT_MENU)
        {

            etat = menuPrincipal(TAILLE_BOUTONS_MENU, fenetre);
            est_initialise = false;
            numeroVague = 0;
            Text texte;
            texte.setFont(font);
            texte.setCharacterSize(24);
            texte.setColor(sf::Color::Red);
            texte.setString(" ");
            if(a_gagne)
            {
                texte.setString(GAGNE);
                texte.setOrigin(texte.getGlobalBounds().width/2, texte.getGlobalBounds().height/2);
                texte.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);
            }
            else if(a_perdu)
            {
                texte.setString(PERDU);
                texte.setOrigin(texte.getGlobalBounds().width/2, texte.getGlobalBounds().height/2);
                texte.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);
            }
                fenetre->draw(texte);

        }
        else if(etat == ETAT_JEU)
        {
            a_gagne = false;
            a_perdu = false;

            if(!est_initialise)
            {
                vague.reset();
                est_initialise = true;
            }
            fenetre->draw(carte);

            while(fenetre->pollEvent(event))
            {
                if(event.type == Event::Closed)
                {
                    etat= ETAT_QUITTER;
                }
                if(event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
                {
                    etat = ETAT_MENU;
                }
                if(etat == ETAT_JEU && event.type == Event::MouseButtonPressed)
                {
                    vague.get_joueur()->clic(event);
                }
                if(etat == ETAT_JEU && event.type == Event::KeyPressed && event.key.code == Keyboard::Space)
                {
                    vague.get_joueur()->espace();
                }

            }
            if(!vague.get_tous_morts())
            {
                vague.dessine();
            }
            else
            {

                numeroVague++;
                vague.vider_ennemis();
                if(numeroVague+1 == 2) //C'est la vague 2
                {
                    vague.add_Ennemi(new Ennemi_Normal(ennemi_21));
                    vague.add_Ennemi(new Ennemi_Normal(ennemi_22));
                    vague.add_Ennemi(new Ennemi_Tank(ennemi_23));
                    vague.add_Ennemi(new Ennemi_Normal(ennemi_24));
                }
                else if(numeroVague+1==3)
                {
                    vague.add_Ennemi(new Ennemi_Degats(ennemi_31));
                    vague.add_Ennemi(new Ennemi_Tank(ennemi_32));
                    vague.add_Ennemi(new Ennemi_Normal(ennemi_33));

                }
                else if(numeroVague+1==4)
                {

                    vague.add_Ennemi(new Ennemi_Degats(ennemi_41));
                    vague.add_Ennemi(new Ennemi_Tank(ennemi_42));
                    vague.add_Ennemi(new Ennemi_Normal(ennemi_43));
                    vague.add_Ennemi(new Ennemi_Tank(ennemi_44));
                }
                else if (numeroVague + 1 ==5)
                {

                    vague.add_Ennemi(new Ennemi_Tank(ennemi_52));
                    vague.add_Ennemi(new Ennemi_Tank(ennemi_53));
                    vague.add_Ennemi(new Ennemi_Degats(ennemi_51));
                    vague.add_Ennemi(new Ennemi_Degats(ennemi_54));
                }
                else
                {
                    a_gagne = true;
                    etat = ETAT_MENU;

                }
                vague.reset();

            }

            if(!vague.get_joueur()->est_vivant())
            {
                a_perdu = true;
                etat = ETAT_MENU;
            }
        }



        fenetre->display();
    }
    while(etat!=ETAT_QUITTER);
    fenetre->close();

    return EXIT_SUCCESS;
}
