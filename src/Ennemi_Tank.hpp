#pragma once
#include "Ennemi.hpp"
#include "Position.hpp"
#include <iostream>
#include <SFML/Graphics.hpp>



// Classe qui représente notre "méchant" qui possede le plus de vie
class Ennemi_Tank : public Ennemi {
	private :


	public :
		// Constructeur
		Ennemi_Tank(sf::RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E);

		// Destructeur virtuel
		virtual ~ Ennemi_Tank(){}

		// Méthode de dessin
		virtual void dessine() override;
};
