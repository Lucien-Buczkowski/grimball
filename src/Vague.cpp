#include "constantes.hpp"
#include "Vague.hpp"
#include <SFML/Graphics.hpp>
#include "Ennemi.hpp"
#include <vector>
#include <iostream>
using namespace std;
using namespace sf;

Vague::Vague(sf::RenderWindow* fenetre,Joueur* joueur, int numero)
    :fenetre_(fenetre), joueur_(joueur), ennemis_( {}), numero_vague_(numero)
{
    sont_tous_morts_ = false;
}

Vague::~Vague()
{
    this->reset();
    delete(joueur_);
    joueur_ = nullptr;
    delete(fenetre_);
    fenetre_ = nullptr;
}

void Vague::reset()
{
    for(size_t i(0); i < ennemis_.size(); ++i)
    {
        ennemis_[i]->recommence();
    }
    joueur_->set_position(Position(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2));
    joueur_->remettre_balle();
    dessine();
}

void Vague::add_Ennemi(Ennemi* ennemi)
{
    ennemi->set_vivant(true);
    ennemis_.push_back(ennemi);
}

void Vague::dessine()
{
    this->evolue();
    joueur_->dessine();
    for(size_t i(0); i < ennemis_.size(); ++i)
    {
        if(ennemis_[i]->est_vivant())
        {
            ennemis_[i]->dessine();
        }
    }

}

void Vague::vider_ennemis()
{
    for(size_t i(0); i < ennemis_.size(); ++i)
    {
        delete(ennemis_[i]);
        ennemis_[i] = nullptr;
    }
    ennemis_.clear();
}

void Vague::set_fenetre(sf::RenderWindow * fenetre)
{
    fenetre_ = fenetre;
    for(size_t i(0); i < ennemis_.size(); ++i)
    {
        ennemis_[i]->set_fenetre(fenetre_);
    }
    joueur_->set_fenetre(fenetre_);
}

void Vague::evolue()
{

    int xBalle;
    int yBalle;
    int xEnnemi;
    int yEnnemi;

    //Gestion des d�placements du joueur en fonction des touches
    if (Keyboard::isKeyPressed(Keyboard::Q))
    {
        joueur_->deplacement('L');
    }
    else if (Keyboard::isKeyPressed(Keyboard::D))
    {
        joueur_->deplacement('R');
    }
    else if (Keyboard::isKeyPressed(Keyboard::Z))
    {
        joueur_->deplacement('U');
    }
    else if (Keyboard::isKeyPressed(Keyboard::S))
    {
        joueur_->deplacement('D');
    }
    else
        joueur_->deplacement('S');

    joueur_->evolue();
    xBalle= joueur_->get_Balle()->get_x();
    yBalle= joueur_->get_Balle()->get_y();
    for(size_t i(0); i < ennemis_.size(); ++i)
    {
        if(ennemis_[i]->est_vivant())
        {
            ennemis_[i]->evolue(joueur_->get_pos_x(), joueur_->get_pos_y());
            xEnnemi= ennemis_[i]->get_pos_x();
            yEnnemi= ennemis_[i]->get_pos_y();

            //Collision de la balle avec l'ennemi
            if(joueur_->get_Balle()->get_sprite().getGlobalBounds().intersects(ennemis_[i]->get_sprite().getGlobalBounds()) && !joueur_->get_possede_la_balle())
            {
                if(xBalle + LARGEUR_BALLE >= xEnnemi
                        && xBalle < xEnnemi + ennemis_[i]->get_taille_x()
                        && yBalle + HAUTEUR_BALLE > yEnnemi
                        && yBalle < yEnnemi + ennemis_[i]->get_taille_y())
                {
                    joueur_->get_Balle()->set_vitesse_x(-joueur_->get_Balle()->get_vitesse_x());
                }
                else if(xBalle + LARGEUR_BALLE > xEnnemi
                        && xBalle <= xEnnemi + ennemis_[i]->get_taille_x()
                        && yBalle + HAUTEUR_BALLE > yEnnemi
                        && yBalle < yEnnemi + ennemis_[i]->get_taille_y())
                {
                    joueur_->get_Balle()->set_vitesse_x(-joueur_->get_Balle()->get_vitesse_x());
                }

                if(xBalle + LARGEUR_BALLE > xEnnemi
                        && xBalle < xEnnemi + ennemis_[i]->get_taille_x()
                        && yBalle + HAUTEUR_BALLE >= yEnnemi
                        && yBalle < yEnnemi + ennemis_[i]->get_taille_y())
                {
                    joueur_->get_Balle()->set_vitesse_y(-joueur_->get_Balle()->get_vitesse_y());
                }
                else if(xBalle + LARGEUR_BALLE > xEnnemi
                        && xBalle < xEnnemi + ennemis_[i]->get_taille_x()
                        && yBalle + HAUTEUR_BALLE > yEnnemi
                        && yBalle <= yEnnemi + ennemis_[i]->get_taille_y())
                {
                    joueur_->get_Balle()->set_vitesse_y(-joueur_->get_Balle()->get_vitesse_y());
                }

                ennemis_[i]->enlever_PV(joueur_->get_PA()); //Enleve un points de vie a l'ennemi si il est touch� par la balle
            }
        }
    }
    Position pos_du_joueur;
    pos_du_joueur = joueur_->get_pos();
    Position decalage_joueur(0.5* joueur_->get_taille_x(), 0.5* joueur_->get_taille_y());
    pos_du_joueur = pos_du_joueur + decalage_joueur;
    bool sont_tous_morts = true;


    for(size_t i(0); i < ennemis_.size(); ++i)  //Boucle pour d�placer les ennemis vers le joueur
    {

        Position pos_de_ennemi;
        pos_de_ennemi = ennemis_[i]->get_pos();
        Position decalage_ennemi(0.5*ennemis_[i]->get_taille_x(), 0.5*ennemis_[i]->get_taille_y());
        pos_de_ennemi = pos_de_ennemi + decalage_ennemi;
        pos_de_ennemi = pos_de_ennemi - pos_du_joueur;
        int distance(pos_de_ennemi.norme());

        if (distance < ennemis_[i]->get_portee() && ennemis_[i]->est_vivant())  //Retrait des points de vie du joueur lors d'une collision avec un ennemi
        {
            joueur_->enlever_PV(ennemis_[i]->get_PA());
        }

        if(sont_tous_morts && ennemis_[i]->est_vivant())
        {
            sont_tous_morts = false;

        }

    }
    sont_tous_morts_ = sont_tous_morts;
}


Joueur* Vague::get_joueur()
{
    return joueur_;
}


bool Vague::get_tous_morts()
{
    return sont_tous_morts_;
}
