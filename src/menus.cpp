#include <SFML/Graphics.hpp>
#include "constantes.hpp"
#include "Position.hpp"
#include "menus.hpp"
#include <stdio.h>

using namespace sf;

int menuPrincipal(int taille_boutons, RenderWindow* fenetre)
{
    int action=0;

    //fond du menu
    Texture textureFondMP;
    textureFondMP.loadFromFile(FOND_MENU_PRINCIPAL);
    Sprite fond_Menu_Principal(textureFondMP);
    fond_Menu_Principal.setPosition(0, 0);
    fenetre->draw(fond_Menu_Principal);

    //bouton jouer
    Bouton_Menu jouer;
    jouer.pos.set_x(LARGEUR_FENETRE/2 - POSITION_BOUTON_JOUER_X);
    jouer.pos.set_y(POSITION_BOUTON_JOUER_Y);


    Texture textureBoutonJouer;
    textureBoutonJouer.loadFromFile(BOUTON_JOUER);
    Sprite jouer_Bouton(textureBoutonJouer);
    jouer_Bouton.setPosition(jouer.pos.valeur_x(), jouer.pos.valeur_y());
    fenetre->draw(jouer_Bouton);

    //bouton quitter
    Bouton_Menu quitter;
    quitter.pos.set_x(LARGEUR_FENETRE/2 - POSITION_BOUTON_QUITTER_X);
    quitter.pos.set_y(POSITION_BOUTON_QUITTER_Y);

    Texture textureBoutonQuitter;
    textureBoutonQuitter.loadFromFile(BOUTON_QUITTER);
    Sprite quitter_Bouton(textureBoutonQuitter);
    quitter_Bouton.setPosition(quitter.pos.valeur_x(), quitter.pos.valeur_y());
    fenetre->draw(quitter_Bouton);

    Event event;

    while(fenetre->pollEvent(event))
    {
        if(event.type==Event::MouseButtonPressed)
        {
            if(event.mouseButton.x > jouer.pos.valeur_x() && event.mouseButton.x < jouer.pos.valeur_x() + (taille_boutons*LONGUEUR_BOUTONS) && event.mouseButton.y > jouer.pos.valeur_y() && event.mouseButton.y < jouer.pos.valeur_y() + (taille_boutons*LARGEUR_BOUTONS) )
                action=ETAT_JEU;

            if(event.mouseButton.x > quitter.pos.valeur_x() && event.mouseButton.x < quitter.pos.valeur_x() + (taille_boutons*LONGUEUR_BOUTONS) && event.mouseButton.y > quitter.pos.valeur_y() && event.mouseButton.y < quitter.pos.valeur_y() + (taille_boutons*LARGEUR_BOUTONS) )
                action=ETAT_QUITTER;
        }
        if(event.type==Event::Closed)
        {
                action=ETAT_QUITTER;
        }
    }

    return action;
}
