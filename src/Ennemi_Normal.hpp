#pragma once
#include "Ennemi.hpp"
#include "Position.hpp"
#include <SFML/Graphics.hpp>


// Classe qui représente notre "méchant" de base
class Ennemi_Normal : public Ennemi {
	private :


	public :
		// Constructeur
		Ennemi_Normal(sf::RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E);

		// Destructeur virtuel
		virtual ~ Ennemi_Normal(){}

		// Méthode de dessin
		virtual void dessine() override;

};
