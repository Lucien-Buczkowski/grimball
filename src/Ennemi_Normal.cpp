#include "Ennemi_Normal.hpp"
#include "Ennemi.hpp"
#include "Position.hpp"
#include "constantes.hpp"
#include <iostream>
#include <SFML/Graphics.hpp>

using namespace sf;

Ennemi_Normal::Ennemi_Normal(RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E)
	:Ennemi(fenetre, pos, texture_chemin_O, texture_chemin_E)
	{
	    vitesse_ = VITESSE_ENNEMI_DE_BASE;
		point_attaque_ = PA_ENNEMI_DE_BASE;
		point_de_vie_ = PV_ENNEMI_DE_BASE; 
		portee_ = PORTEE_ENNEMI_DE_BASE;
		taille_x_=TAILLE_ENNEMI_DE_BASE;
		taille_y_=TAILLE_ENNEMI_DE_BASE;
		nom_ ="Normal";
		est_en_vie_ =true;
	}

void Ennemi_Normal::dessine(){
	if(est_en_vie_){
		sprite_du_personnage_.setPosition(pos_.valeur_x(), pos_.valeur_y());
		fenetre_->draw(sprite_du_personnage_);
	}
}
