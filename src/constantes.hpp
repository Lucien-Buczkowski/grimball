#pragma once
// Dans ce fichier on met toutes les constantes de notre Jeux
// Ceci est pour un test

//------------valeurs par d�faut------------------
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 720

#define ATTENTE_ANIMATIONS 10


#define LARGEUR_MUR 24
#define LARGEUR_BOUTONS 1
#define LONGUEUR_BOUTONS 2

//-----------------musique--------------------
#define THEME_PCPAL "../ressources/musiques/epic_win.ogg"




//-------------------joueur-----------------
#define LARGEUR_JOUEUR 64
#define HAUTEUR_JOUEUR 128
#define PAS_DEPLACEMENT 5
#define TAILLE_JOUEUR 0.5
#define PV_JOUEUR 180
#define PA_JOUEUR 1

//-----------------ennemi_tank----------------
#define TAILLE_TANK 88
#define VITESSE_TANK 1
#define PORTEE_TANK 40
#define PA_TANK 1
#define PV_TANK 6

//-----------------ennemi_base ----------------
#define TAILLE_ENNEMI_DE_BASE 60
#define VITESSE_ENNEMI_DE_BASE 2
#define PORTEE_ENNEMI_DE_BASE 50
#define PA_ENNEMI_DE_BASE 1
#define PV_ENNEMI_DE_BASE 2

//-----------------ennemi_degats ----------------
#define TAILLE_ENNEMI_DEGATS 64
#define VITESSE_ENNEMI_DEGATS 2
#define PORTEE_ENNEMI_DEGATS 50
#define PA_ENNEMI_DEGATS 3
#define PV_ENNEMI_DEGATS 2


//------------------balle----------------
#define LARGEUR_BALLE 32
#define HAUTEUR_BALLE 32
#define RAYON_BALLE 16



//-------------------------menu-----------------------
#define POSITION_BOUTON_JOUER_X 100
#define POSITION_BOUTON_JOUER_Y 200
#define POSITION_BOUTON_QUITTER_X 100
#define POSITION_BOUTON_QUITTER_Y 500
#define TAILLE_BOUTONS_MENU 100




//------------------textures------------------
#define BOUTON_JOUER "../ressources/boutons/boutonJouerV2.png"
#define BOUTON_QUITTER "../ressources/boutons/boutonQuitterV2.png"
#define PERSONNAGE_DE_FACE_AU_DEBUT "../ressources/personnage/character_face_1.png"

#define PROTOTYPE_PERSONNAGE_DOS "../ressources/personnage/character_back_"
#define PROTOTYPE_PERSONNAGE_FACE "../ressources/personnage/character_face_"
#define PROTOTYPE_PERSONNAGE_DROITE "../ressources/personnage/character_side_right_"
#define PROTOTYPE_PERSONNAGE_GAUCHE "../ressources/personnage/character_side_left_"

#define ENNEMI_NORMAL_DROITE "../ressources/ennemis/ennemi_basique_droite.png"
#define ENNEMI_NORMAL_GAUCHE "../ressources/ennemis/ennemi_basique_gauche.png"
#define ENNEMI_TANK_DROITE "../ressources/ennemis/ennemi_tank_droite_1.png"
#define ENNEMI_TANK_GAUCHE "../ressources/ennemis/ennemi_tank_gauche_1.png"
#define ENNEMI_DEGATS_GAUCHE "../ressources/ennemis/ennemi_degats_gauche.png"
#define ENNEMI_DEGATS_DROITE "../ressources/ennemis/ennemi_degats_droite.png"

#define TEXTURE_CARTE "../ressources/map/map.png"
#define FOND_MENU_PRINCIPAL "../ressources/ecran_menu/ecran_menu.png"
#define BALLE "../ressources/balle/balle.png"

#define BARRE_DE_VIE_3HP "../ressources/personnage/barre_de_vie_1.png"
#define BARRE_DE_VIE_2HP "../ressources/personnage/barre_de_vie_2.png"
#define BARRE_DE_VIE_1HP "../ressources/personnage/barre_de_vie_3.png"
#define BARRE_DE_VIE_0HP "../ressources/personnage/barre_de_vie_4.png"
#define POS_X_BARRE_PV 50
#define POS_Y_BARRE_PV 30


//-------------------etats----------------------
#define ETAT_QUITTER 1
#define ETAT_MENU 0
#define ETAT_JEU 2


#define PERDU "Vous avez perdu"
#define GAGNE "Vous avez gagne"
#define POLICE "../ressources/8-BIT WONDER.ttf"
