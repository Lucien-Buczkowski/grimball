#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "Ennemi.hpp"
#include "Joueur.hpp"
#include "Balle.hpp"


// Classe qui représente les vagues de notre jeu
class Vague {
	private :
	// possede un pointeur sur un RenderWindow
	sf::RenderWindow* fenetre_;
	// possede une collection héterogene d'Ennemi
	// donc on a un tableau de pointeur sur des Ennemis (On utilise le template vector pour les tableaus en C++)
	std::vector<Ennemi*> ennemis_;
	// possede un pointeur sur un Joueur
	Joueur* joueur_;
	// possede un numero de vague
	int numero_vague_;
	bool sont_tous_morts_;

	public :
	// Constructeur
	Vague(sf::RenderWindow* fenetre ,Joueur* joueur, int numero);
	
	// Destructeur
	~Vague();

	// On ajoute un Ennemi a la Vague
	void add_Ennemi(Ennemi* ennemi);

	// getter pour le joueur
	Joueur* get_joueur();

	// Pour dessiner une vague (va faire appel a Joueur::dessine() et Ennemi::dessine())
	void dessine();

	// vide le tableau d'ennemi
	void reset();

	// pour faire evoluer tout ce beau monde
	void evolue();

	// méthode pour savoir si tous les ennemis sont morts
	bool get_tous_morts();
	
	// méthode pour changer la fenetre de dessin
	void set_fenetre(sf::RenderWindow * fenetre);
	
	// méthode pour vider le tableau des ennemis
	void vider_ennemis();
};
