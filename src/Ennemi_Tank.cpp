#include "Ennemi_Tank.hpp"
#include "Ennemi.hpp"
#include "Position.hpp"
#include "constantes.hpp"
#include <iostream>
#include <SFML/Graphics.hpp>

using namespace sf;

Ennemi_Tank::Ennemi_Tank(RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E)
	:Ennemi(fenetre, pos, texture_chemin_O, texture_chemin_E)
	{
		
		vitesse_ = VITESSE_TANK;
		point_de_vie_ = PV_TANK;
		point_attaque_ = PA_TANK;
		taille_x_=TAILLE_TANK;
		taille_y_=TAILLE_TANK;
		portee_ = PORTEE_TANK;
		nom_ ="Tank";
		est_en_vie_ =true;
		
	}

void Ennemi_Tank::dessine(){
	if(est_en_vie_){
		sprite_du_personnage_.setPosition(pos_.valeur_x(), pos_.valeur_y());
		fenetre_->draw(sprite_du_personnage_);
	}
}
