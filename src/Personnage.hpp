#pragma once
#include <SFML/Graphics.hpp>
#include "Position.hpp"
#include <string>

// Classe (abstraite) représantant le concetp de personnage
class Personnage {
	//attributs qui pourrons êttre utiliser pour les sous classes
	protected :
		Position pos_;
        Position pos_initiale_;
		std::string nom_;
		int point_de_vie_;
		int point_attaque_;
		int portee_; 
		int vitesse_;
		int taille_x_;
		int taille_y_;
		bool est_en_vie_;
		//Texture du sprite
		sf::Texture texture_du_personnage_;
		// C'est l'"image" du personnage
		sf::Sprite sprite_du_personnage_;
		// fenetre que l'on donne au personnage pour qu'il se dessine
		sf::RenderWindow* fenetre_;


	public :
		// Constructeur 
		Personnage(sf::RenderWindow* fenetre, Position pos);

		// Destructeur qui doit être virtual
		virtual ~Personnage() {}

		// série de getters accéder à certains attributs
		Position get_pos() ;
		int get_pos_x() ; 
		int get_pos_y() ;
		// pour récupérer le nom
		std::string get_nom() ;
		// pour récupérer la vie
		int get_PDV() ;
		// pour récupérer l'attaque
		int get_PA() ;
		// pour récupérer la longueur du Personnage
		int get_taille_x() ;
		// pour récupérer la hauteur du Personnage
		int get_taille_y() ;
		// pour récupérer la porté d'attaque ou de "grab" du Personnage
		int get_portee() ;
		// pour récupérer la vitesse d'un personnage
		int get_vitesse() ;
		// pour récupérer est_en_vie_
		bool est_vivant() ;
		
		// setter pour est_en_vie_
		void set_vivant(bool vie);
		
		// setter pour enlever des PV
		void enlever_PV(int degat);

		// setter pour choisir où on va le dessiner
		void set_fenetre(sf::RenderWindow* fenetre);
		
		// getter pour savoir où on dessine
		sf::RenderWindow* get_fenetre();
		
		// getter pour savoir où est la position initiale
		Position get_position_initiale();

		// fonction pour dessiner le personnage (on ne sait pas a quoi ressemble celui-ci)
		// => méthode virtuelle pure
		virtual void dessine() =0;
		
		// getter du sprite
		sf::Sprite get_sprite();
};
