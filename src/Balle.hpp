#pragma once
#include "Position.hpp"
#include <SFML/Graphics.hpp>
#include "Ennemi.hpp"
// Classe qui représente une balle.
class Balle {
	private :
		Position pos_;
		int rayon_;
		int vitesse_x_;
		int vitesse_y_;
		sf::Sprite sprite_balle_;
		int cote_collision(sf::Sprite referentiel, sf::Sprite contact);
		sf::RenderWindow* fenetre_;


	public :
		// Constructeur
		Balle(sf::RenderWindow* fenetre, Position pos);
		
		// Getter et setter pour la vitesse
		int get_vitesse_x();
		int get_vitesse_y();
		void set_vitesse_x(int vit_x);
		void set_vitesse_y(int vit_y);
		
		// getter pour la Position
		Position get_position();
		
		// méthode pour gerer les déplacements de la balle
		void evolue();
		
		// méthode pour set la position de la balle
		void set_Position(int x, int y);
		
		// méthode pour afficher la balle
		void dessine();
		
		// getter pour la position en x
		int get_x();
		
		// getter pour la position en x
		int get_y();
		
		// getter pour le sprite
		sf::Sprite get_sprite();
};
