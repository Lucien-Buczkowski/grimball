#pragma once
#include "Personnage.hpp"
#include "Balle.hpp"
#include "Position.hpp"
#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;

// Classe représentant le personnage controlé par l'utilisateur du jeu
class Joueur : public Personnage {
	private :
		
		bool possede_la_balle_;
		Balle* balle_;
        int etat_;
        int attente_;
        char direction_;
        bool clic_;
        bool espace_;
        Position position_souris_;
		std::string nom_fichier_texture(char direction, int etat);
		sf::Texture texture_barre_pv;
		sf::Sprite sprite_vie_;


	public :
		// Constructeur
		Joueur(sf::RenderWindow* fenetre, Position pos, Balle* balle);
		
		// Destructeur
		virtual ~Joueur() {}
		
		// méthode pour faire le lien entre saisies clavier et déplacement du joueur
		void deplacement(char direction);
		
		//méthode pour dessiner le joueur
		virtual void dessine() override;
		
		// méthode pour avoir accès a la balle du jouoeur (en dehors du joueur)
		Balle* get_Balle();
		
		// setter pour la balle du joueur
		void set_balle(Balle* balle);
		
		// méthode pour faire évoluer le personnage(déplacements et lancer de balle)
		void evolue();
		
		// méthode pour savoir si l'utilisateur a cliqué ou non
		void clic(Event event);
		
		// methode pour savoir si l'utilisateur a appuyé sur la touche espace
		void espace();
		
		// méthode pour savoir si le joueur possède la balle sur lui
		bool get_possede_la_balle();
		
		// méthode pour modifier la position du Joueur
		void set_position(Position position);
		
		// méthode pour redonner la balle au joueur
		void remettre_balle();
};
