#pragma once
#include "Ennemi.hpp"
#include "Position.hpp"
#include <SFML/Graphics.hpp>
#include <iostream>

// Classe qui représente notre "méchant" qui fait le plus de degat
class Ennemi_Degats : public Ennemi {
	private :


	public :
		// Constructeur
		Ennemi_Degats(sf::RenderWindow* fenetre, Position pos, std::string texture_chemin_O, std::string texture_chemin_E);

		// Destructeur virtuel
		virtual ~ Ennemi_Degats(){}

		// Méthode de dessin
		virtual void dessine() override;

};
