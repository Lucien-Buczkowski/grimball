#include "Joueur.hpp"
#include "constantes.hpp"
#include "Personnage.hpp"
#include "Position.hpp"
#include "Balle.hpp"
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include <cmath>

using namespace sf;

Joueur::Joueur(RenderWindow* fenetre, Position pos, Balle* balle)
    :Personnage(fenetre, pos)
{
    possede_la_balle_ = true;
    Texture texture;
    texture.loadFromFile(PERSONNAGE_DE_FACE_AU_DEBUT);
    texture_du_personnage_ = texture;
    sprite_du_personnage_=Sprite(texture_du_personnage_);
    sprite_du_personnage_.scale(TAILLE_JOUEUR, TAILLE_JOUEUR);
    etat_ = 1;
    clic_ = false;
    espace_ = false;
    position_souris_ = Position();
    balle_ = balle;
    attente_ = 0;
    direction_ = 'D';
    portee_ = 10;
    point_attaque_ = PA_JOUEUR;
    point_de_vie_ = PV_JOUEUR;
    taille_x_=LARGEUR_JOUEUR;
    taille_y_=HAUTEUR_JOUEUR;
    nom_ ="Joueur";
    est_en_vie_ =true;
    sprite_du_personnage_.setPosition(pos_.valeur_x(), pos_.valeur_y());
}
void Joueur::deplacement(char direction)
{
    if(direction == 'U' && pos_.valeur_y()>0)
    {
        pos_.set_y(pos_.valeur_y()-PAS_DEPLACEMENT);
        direction_ = 'U';
    }
    if(direction == 'D' && pos_.valeur_y()<HAUTEUR_FENETRE-HAUTEUR_JOUEUR*TAILLE_JOUEUR-LARGEUR_MUR*2)
    {
        pos_.set_y(pos_.valeur_y()+PAS_DEPLACEMENT);
        direction_ = 'D';
    }
    if(direction == 'R' && pos_.valeur_x()<LARGEUR_FENETRE-LARGEUR_MUR-LARGEUR_JOUEUR*TAILLE_JOUEUR)
    {
        pos_.set_x(pos_.valeur_x()+PAS_DEPLACEMENT);
        direction_ = 'R';
    }
    if(direction == 'L' && pos_.valeur_x()>LARGEUR_MUR)
    {
        pos_.set_x(pos_.valeur_x()-PAS_DEPLACEMENT);
        direction_ = 'L';
    }
    if (direction == 'S')
        direction_= 'S';

    sprite_du_personnage_.setPosition(pos_.valeur_x(),pos_.valeur_y());
}

void Joueur::clic(Event event)
{
    position_souris_ = Position(event.mouseButton.x, event.mouseButton.y);
    clic_ = true;
}

void Joueur::espace()
{
    espace_ = true;
}

bool Joueur::get_possede_la_balle(){
    return possede_la_balle_;
}

void Joueur::remettre_balle(){
    possede_la_balle_ = true;
    balle_->set_Position(100000, 100000);
    balle_->set_vitesse_x(0);
    balle_->set_vitesse_y(0);
    point_de_vie_ = PV_JOUEUR;
    est_en_vie_ = true;
    evolue();
}

void Joueur::set_position(Position position)
{
    pos_ = position;
}

void Joueur::dessine()
{
    if(!possede_la_balle_)
        balle_->dessine();
    if(est_en_vie_){
    texture_du_personnage_.loadFromFile(nom_fichier_texture(direction_, etat_));
    sprite_du_personnage_.setTexture(texture_du_personnage_);
    fenetre_->draw(sprite_du_personnage_);
    if(attente_ == 0)
    {
        etat_++;
        if(etat_ > 4)
            etat_ = 1;
    }
    attente_++;
    if(attente_ > ATTENTE_ANIMATIONS)
        attente_ = 0;


    if(point_de_vie_>120)
        texture_barre_pv.loadFromFile(BARRE_DE_VIE_3HP);
    else if(point_de_vie_>60)
        texture_barre_pv.loadFromFile(BARRE_DE_VIE_2HP);
    else if(point_de_vie_>0)
        texture_barre_pv.loadFromFile(BARRE_DE_VIE_1HP);
    else
        texture_barre_pv.loadFromFile(BARRE_DE_VIE_0HP);

     sprite_vie_.setTexture(texture_barre_pv);
     sprite_vie_.setPosition(POS_X_BARRE_PV,POS_Y_BARRE_PV);
     fenetre_->draw(sprite_vie_);
}
}

std::string Joueur::nom_fichier_texture(char direction, int etat)
{
    switch(direction)
    {
    case 'U':
        return PROTOTYPE_PERSONNAGE_DOS+std::to_string(etat)+".png" ;
    case 'D':
        return PROTOTYPE_PERSONNAGE_FACE+std::to_string(etat)+".png" ;
    case 'R':
        return PROTOTYPE_PERSONNAGE_DROITE+std::to_string(etat)+".png" ;
    case 'L':
        return PROTOTYPE_PERSONNAGE_GAUCHE+std::to_string(etat)+".png" ;
    case 'S':
        return PROTOTYPE_PERSONNAGE_FACE+std::to_string(1)+".png" ;
    }
}

void Joueur::evolue()
{

    if(!possede_la_balle_){
        balle_->evolue();
    }

    if(possede_la_balle_)
    {
        balle_->set_Position(pos_.valeur_x(), pos_.valeur_y());

        if(clic_)
        {

            double X = (position_souris_.valeur_x() - pos_.valeur_x());
            double Y = (position_souris_.valeur_y() - pos_.valeur_y());

            double norme(sqrt(X*X + Y*Y));

            X = (X/norme)*10;
            Y = (Y/norme)*10;
			int x(X);
            int y(Y);
            X = X  +0.5 - (X<0);
            Y = Y +0.5 - (Y<0);

            x = X;
            y = Y;

            balle_->set_vitesse_x(x);
            balle_->set_vitesse_y(y);
            possede_la_balle_ = false;
        }


    }
    else if(espace_)
    {
        if(balle_->get_sprite().getGlobalBounds().intersects(sprite_du_personnage_.getGlobalBounds()))
        {
            possede_la_balle_ = true;
        }

    }
            espace_ = false;

            clic_ = false;

}

Balle* Joueur::get_Balle()
{
    return balle_;
}

void Joueur::set_balle(Balle* balle)
{
    balle_ = balle;
}
